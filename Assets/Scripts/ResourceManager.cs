﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

public class ResourceManager : MonoBehaviour {

    private class MyTexture {
        public string name;
        public string url;
        public byte[] bytes;
        
        public MyTexture(string name,string url) {
            this.name = name;
            this.url = url;
        }

        public MyTexture(string name,byte[] bytes) {
            this.name = name;
            this.bytes = bytes;
        }
    }

    public Dictionary<string,Texture> loadedTextures;

    [SerializeField] private int threadPoolSize;
    [SerializeField] private string[] textureNames;
    [SerializeField] private string[] textureUrls;

    private Queue<MyTexture> downloadingTextures;
    private Queue<MyTexture> loadingTextures;
    private Queue<MyTexture> savingTextures;
    private Queue<MyTexture> convertingTextures;

    private delegate void TextureDelegate(string name,Texture texture);

    private string folderPath;

    private int freeThread;

    void Start () {
        folderPath = Application.persistentDataPath + "/";
        Debug.Log("path : " + folderPath);
        CheckTextures();
        DownloadNextTexture();
        LoadTextures();
    }

    void FixedUpdate () {
        while (convertingTextures.Count > 0) {
            ConvertTexture(convertingTextures.Dequeue());
        }
        while(freeThread > 0 && loadingTextures.Count > 0) {
           Thread thread = new Thread(new ThreadStart(LoadTextureStart));
            thread.Start();
            freeThread--;
        }
        while(freeThread > 0 && savingTextures.Count > 0) {
            Thread thread = new Thread(new ThreadStart(SaveTextureStart));
            thread.Start();
            freeThread--;
        }
    }

    private void CheckTextures() {
        downloadingTextures = new Queue<MyTexture>();
        loadingTextures = new Queue<MyTexture>();
        savingTextures = new Queue<MyTexture>();
        convertingTextures = new Queue<MyTexture>();
        loadedTextures = new Dictionary<string,Texture>();
        for(int i = 0;i < textureNames.Length;i++) {
            if(File.Exists(folderPath + textureNames[i] + ".png")) {
                loadingTextures.Enqueue(new MyTexture(textureNames[i],textureUrls[i]));
            } else {
                downloadingTextures.Enqueue(new MyTexture(textureNames[i],textureUrls[i]));
            }
        }
    }

    private void DownloadNextTexture() {
        if (downloadingTextures.Count > 0)
            StartCoroutine(DownloadTexture(downloadingTextures.Dequeue(),DownloadCallBack));
    }

    private IEnumerator DownloadTexture(MyTexture myTexture,TextureDelegate callBack) {
        WWW www = new WWW(myTexture.url);
        www.threadPriority = UnityEngine.ThreadPriority.High;
        yield return www;
        callBack(myTexture.name,www.texture);
    }

    private void DownloadCallBack(string name,Texture texture) {
        Debug.Log(name + " downloaded");
        DownloadNextTexture();
        MyTexture myTexture = new MyTexture(name,((Texture2D)texture).EncodeToPNG());
        savingTextures.Enqueue(myTexture);
        loadedTextures.Add(name,texture);
        Debug.Log(name + " loaded");
    }

    private void LoadTextures() {
        freeThread = threadPoolSize;
        while(freeThread > 0 && loadingTextures.Count > 0) {
           Thread thread = new Thread(new ThreadStart(LoadTextureStart));
            thread.Start();
            freeThread--;
        }
    }

    private void SaveTextureStart() {
        MyTexture myTexture = null;
        lock (savingTextures) {
            if(savingTextures.Count > 0)
                myTexture = savingTextures.Dequeue();
        }
        if(myTexture != null)
            SaveTexture(myTexture);
        freeThread++;
    }

    private void SaveTexture(MyTexture myTexture) {
        string path = folderPath + myTexture.name + ".png";
        File.WriteAllBytes(path,myTexture.bytes);
        Debug.Log(myTexture.name + " saved");
    }

    private void LoadTexture(MyTexture myTexture) {
        byte[] bytes;
        bytes = System.IO.File.ReadAllBytes(folderPath + myTexture.name + ".png");
        myTexture.bytes = bytes;
        convertingTextures.Enqueue(myTexture);
    }

    private void LoadTextureStart() {
        MyTexture myTexture = null;
        lock (loadingTextures) {
            if(loadingTextures.Count > 0)
                myTexture = loadingTextures.Dequeue();           
        }
        if (myTexture != null)
            LoadTexture(myTexture);
        freeThread++;
    }

    private void ConvertTexture(MyTexture myTexture) {
        Texture2D texture2D = new Texture2D(512,512);
        texture2D.LoadImage(myTexture.bytes);
        loadedTextures.Add(myTexture.name,texture2D);
        Debug.Log(myTexture.name + " loaded");
    }
}
