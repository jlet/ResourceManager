﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureLoader : MonoBehaviour {

    [SerializeField] private string textureName;

    bool loaded;

    // Use this for initialization
    void Start () {
        loaded = false;     
    }
	
	// Update is called once per frame
	void Update () {
        if(!loaded && GameObject.FindGameObjectWithTag("ResourceManager").GetComponent<ResourceManager>().loadedTextures.ContainsKey(textureName)) {
            Texture texture;
            GameObject.FindGameObjectWithTag("ResourceManager").GetComponent<ResourceManager>().loadedTextures.TryGetValue(textureName,out texture);
            Rect rect = new Rect(0,0,texture.width,texture.height);
            Sprite sprite = Sprite.Create((Texture2D)texture,rect,new Vector2(0.5f,0.5f));
            GetComponent<SpriteRenderer>().sprite = sprite;
            loaded = true;
        }
    }
}
